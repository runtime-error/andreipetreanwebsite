<!DOCTYPE html>
<html>
<head>
    <title>Portofolio</title>
    <link rel="stylesheet" type="text/css" href="Styles/portofolio.css" />
    <link rel="stylesheet" type="text/css" href="JosefinSans/stylesheet.css" />
</head>
<body>
	<a href="index.html">Home</a>
	<a href="about.html">About</a>
  <a href="contact.html">Contact</a>
    <!-- Header -->
    <div class="header">
        <h1>Portofolio</h1>
        <p>Take a look at my work.</p>
    </div>

    
    <script> 
    // function save_album(i_d)
    // {
    //   const fs = require('fs') 
    //   let data = "i_d"
    //   fs.writeFile('newfile.txt', data, (err) => { 
    //       if (err) throw err; 
    //   }) 
    // }

    function save_local(i_d)
    {
        const album = i_d
        window.localStorage.setItem('album', i_d)
    }
    </script> 
  
    <div class="container">
    <?php
     
    $image_list = array("", "", "", "");
    $albums = scandir('Albums/');
    $cont = 0;
    foreach($albums as $album) {
        if($album !== "." && $album !== "..") {
            $cont = $cont +1;
            $image_list[($cont%4)] .= "<a href= \"gallery.php\"><img src=\"Albums/$album/thumbnail.jpg\" id=\"Albums/$album\" onClick=\"save_local(this.id)\"></a>";

            // $myfile = fopen("newfile.txt", "a") or die("Unable to open file!");
            // $txt = "Albums/$album\n";
            // fwrite($myfile, $txt);
            // fclose($myfile);
      

            // echo "<div class=\"album\">";
            // echo "<img src=\"Albums/$album/thumbnail.jpg\">";
            // echo "<div class=\"album_description\">Description (preferably small)</div>";
            // echo "</div>";
        }
    }

    echo "<div class='column' id = 'col0'>$image_list[0]</div>";
    echo "<div class='column' id = 'col1'>$image_list[1]</div>";
    echo "<div class='column' id = 'col2'>$image_list[2]</div>";
    echo "<div class='column' id = 'col3'>$image_list[3]</div>";
    
    ?>
    </div>
</body>
</html>